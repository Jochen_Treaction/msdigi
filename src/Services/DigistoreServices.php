<?php


namespace App\Services;


use App\Exceptions\DigistoreException;
use DigistoreApi;
use Psr\Log\LoggerInterface;

class DigistoreServices
{
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    /**
     * @var DigistoreApi
     * @author Pradeep
     */
    private $digistore_api;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $encoded_string
     * @return array
     * @author Pradeep
     */
    public function validateRequest(string $encoded_string): array
    {
        $req_arr = [];
        if (empty($encoded_string)) {
            return $req_arr;
        }
        $req_json = base64_decode($encoded_string);
        $req_arr = json_decode($req_json, true, 512, JSON_UNESCAPED_SLASHES);
        return $req_arr;
    }

    /**
     * @param array $digistore_params
     * @return string
     * @author Pradeep
     */
    public function getBuyerUrl(array $digistore_params): string
    {
        $buyer_url = '';
        if (empty($digistore_params) || !isset($digistore_params[ 'apikey' ])) {
            return $buyer_url;
        }
        try {
            $buy_url_params = $this->getBuyUrlParams($digistore_params);
            $digistore_api  = $this->connect($digistore_params[ 'apikey' ]);
            $data = $digistore_api->__call('createBuyUrl', $buy_url_params);
            $buyer_url = $data->url;
            $digistore_api->disconnect();
            return $buyer_url;
        } catch (DigistoreException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__,__LINE__]);
            return $buyer_url;
        }
    }

    /**
     * @param array $digistore_params
     * @return array
     * @author Pradeep
     */
    private function getBuyUrlParams(array $digistore_params) :array
    {
        $product_id['product_id'] = $digistore_params[ 'product_id' ] ?? '';
        $buyer['buyer'] = [
            'email' => $digistore_params[ 'email' ] ?? '',
            'first_name' => $digistore_params[ 'first_name' ] ?? '',
            'last_name' => $digistore_params[ 'last_name' ] ?? '',
            'readonly_keys' => 'email_and_name',
        ];
        $payment_plan['payment_plan'] = [
            'first_amount' => $digistore_params[ 'first_amount' ] ?? '',
            'other_amounts' => $digistore_params[ 'other_amounts' ] ?? '',
            'first_billing_interval' => $digistore_params[ 'first_billing_interval' ] ?? '',
            'other_billing_intervals' => $digistore_params[ 'other_billing_intervals' ] ?? '',
        ];
        $tracking['tracking'] = [];
        $valid_until['valid_until'] = $digistore_params[ 'valid_until' ] ?? '';
        $urls['urls'] = [
            'fallback_url' => $digistore_params[ 'fallback_url' ] ?? '',
        ];
        $placeholders['placeholders'] = [];
        $settings['settings'] = [];

        return [
            '0' => $product_id,
            '1' => $buyer,
            '2' => $payment_plan,
            '3' => $tracking,
            '4' => $valid_until,
            '5' => $urls,
            '6' => $placeholders,
            '7' => $settings
        ];
    }

    /**
     * @param string $api_key
     * @return DigistoreApi
     * @author Pradeep
     */
    public function connect(string $api_key): DigistoreApi
    {
        $digistore_api = '';

        if (isset($api_key) && !empty($api_key)) {
            try {
                $digistore_api = DigistoreApi::connect($api_key);
            } catch (DigistoreException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
            }

        }
        return $digistore_api;
    }

    /**
     * @param string $api_key
     * @return bool
     * @author Pradeep
     */
    public function ping(string $api_key) : bool
    {
        $status = false;
        if(empty($api_key)) {
            return $status;
        }
        $digistoreApi = $this->connect($api_key);
        if(empty($digistoreApi)) {
            return $status;
        }
        try {
            $server_time = $digistoreApi->__call('ping', []);
        } catch (DigistoreException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        if(!empty($server_time)) {
            $status = true;
        }
        return $status;

    }

    /**
     * @return void
     * @author Pradeep
     */
    private function disconnect(): void
    {
        $this->digistore_api = false;
    }

}