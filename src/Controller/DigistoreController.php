<?php


namespace App\Controller;


use App\Exceptions\DigistoreException;
use App\Services\DigistoreServices;
use App\Services\ToolServices;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DigistoreController extends AbstractController
{

    /**
     * Route No 1
     * @Route("/product/buyurl/get",name="get-product-buyurl",methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param DigistoreServices $digistoreServices
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @author Pradeep
     */
    public function getProductBuyUrl(
        Request $request,
        ToolServices $toolServices,
        DigistoreServices $digistoreServices,
        LoggerInterface $logger
    ): JsonResponse {
        try {
            $content = $toolServices->decrypt($request->getContent());
            if (empty($content) || empty($content[ 'apikey' ])) {
                throw new DigistoreException('Invalid Request Or Invalid APIKey');
            }
            $buyer_url = $digistoreServices->getBuyerUrl($content);
            if (empty($buyer_url)) {
                throw new DigistoreException('Failed to get BuyUrl');
            }
            return $this->json(['status' => true, 'message' => 'Successfully fetched BuyUrl', 'content' => $buyer_url]);
        } catch (DigistoreException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => $e->getMessage(), 'content' => $buyer_url]);
        }
    }

    /**
     * @Route("/apikey/check",name="apikey-check",methods={"POST"})
     * @param Request $request
     * @param DigistoreServices $digistoreServices
     * @param LoggerInterface $logger
     * @param ToolServices $toolServices
     * @return JsonResponse
     * @author Pradeep
     */
    public function checkAPIKey(
        Request $request,
        DigistoreServices $digistoreServices,
        LoggerInterface $logger,
        ToolServices $toolServices
    ): JsonResponse {
        try {
            $content = $toolServices->decrypt($request->getContent());
            if (empty($content) || empty($content[ 'apikey' ]) || !$digistoreServices->ping($content[ 'apikey' ])) {
                throw new DigistoreException('Invalid APIKey');
            }
            return $this->json(['status' => true, 'message' => 'APIKey is validated Successfully', 'content' => '']);
        } catch (DigistoreException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => $e->getMessage(), 'content' => '']);
        }
    }


    /**
     * @Route("/check/alive", methods={"GET"})
     */
    public function checkAlive(Request $request)
    {
        return $this->json([
            'alive' => true,
            'your IP' => $request->getClientIp(),
            'toUrl' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]",
        ]);
    }
}
